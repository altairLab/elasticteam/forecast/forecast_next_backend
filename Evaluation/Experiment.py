from shutil import ExecError
import numpy as np
from scipy import signal

import json

import math
import pandas as pd
import matplotlib.pyplot as plt

class Experiment:
    # csv file containing the data of this experiment
    filename = ""

    # environment stiffness
    ke = 0
    # environment inertia
    je = 0

    # parameters dictionary of this experiment
    parameters = {}

    table = pd.DataFrame()

    step1 = pd.DataFrame()
    step2 = pd.DataFrame()
    step3 = pd.DataFrame()
    step4 = pd.DataFrame()
    sweep = pd.DataFrame()

    settling_time = 0.0
    ts = 0.0
    error = {
        "mean": None,
        "std": None
    }
    dynamic_error = {
        "mean": None,
        "std": None
    }
    performance_indicators = {
        "linearity": None,
        "static_error": None,
        "overshoot": None,
        "highfidelity_bandwidth": None,
        "bandwidth": None,
    }
    step_analysis = {
        "settling_time": None,
        "regime_value": None,
        "delay_time": None,
        "rising_time": None,
        "bandwidth": None
    }

    def __init__(self, filename: str, parameters: dict, ke: float, je: float) -> None:
        self.filename = filename
        self.parameters = self._get_experiment_parameters(parameters=parameters)

        self._read_csv(filename=filename)

        self.ke = ke
        self.je = je

        self._split_signals()
        self._compute_settling_time(cutoff_freq=10)
        self._compute_static_error()
        self._compute_error()
        self._compute_linearity()
        self._compute_overshoot()
        self._compute_dynamic_error()
        self._compute_step_analysis(cutoff_freq=10)
        self._compute_highfidelity_bandwidth()


    def _get_experiment_parameters(self, parameters: dict):
        exp_params = {}
        for p in parameters:
            if type(parameters[p]) == dict:
                exp_params[p] = self._get_experiment_parameters(parameters[p])
            else:
                exp_params[p] = parameters[p]
        return exp_params


    def _read_csv(self, filename: str):
        # read the table
        table = pd.read_csv(filename)

        # rename the column 'time' to 't' (if present)
        table.rename({'time': 't'},inplace=True,axis='columns')

        # remove the leading and trailing spaces in the columns names
        for x in list(table.columns):
            table.rename({x: x.strip()},inplace=True,axis="columns")

        # the required names for the evaluation
        required_names = ['t', 'ref', 'tau']

        # remove the non-necessary columns
        table = table[table.columns.intersection(required_names)]

        # check if the remaining columns are the required ones
        if len(required_names) != len(table.columns):
            raise ExecError("Missing parameters " + str(list(table.columns))
                + ", required " + str(required_names))

        # remove the first t from all the times (so it starts with 0)
        table['t'] = table['t'] - table['t'].iloc[0]
        table = table.reset_index()

        # set the sampling time
        self.ts = table['t'].diff().mean()

        # set the table
        self.table = table


    def _split_signals(self):
        ts = self.ts
        step_duration = self.parameters["environment_reference_signal"]["step_duration"]

        t1 = 0
        t2 = math.floor(step_duration / ts)
        self.step1 = self.table[t1:t2].reset_index()

        t1 = t2 # + 1
        t2 = math.floor(2.0*step_duration / ts)
        self.step2 = self.table[t1:t2].reset_index()

        t1 = t2 # + 1
        t2 = math.floor(3.0*step_duration / ts)
        self.step3 = self.table[t1:t2].reset_index()

        t1 = t2 # + 1
        t2 = math.floor(4.0*step_duration / ts)
        self.step4 = self.table[t1:t2].reset_index()

        t1 = t2 # + 1
        t2 = self.table['t'].shape[0]-1
        self.sweep = self.table[t1:t2].reset_index()

        # plt.plot(self.step1['t'],self.step1['ref'])
        # plt.show()
        # plt.plot(self.step2['t'],self.step2['ref'])
        # plt.show()
        # plt.plot(self.step3['t'],self.step3['ref'])
        # plt.show()
        # plt.plot(self.step4['t'],self.step4['ref'])
        # plt.show()
        # plt.plot(self.sweep['t'],self.sweep['ref'])
        # plt.show()


    def _compute_settling_time(self, cutoff_freq: float = 10):
        ts = self.ts

        # low pass filter 2-nd order
        half_sampling_freq = float(1/(2*ts))
        [B,A] = signal.butter(2,cutoff_freq/half_sampling_freq)

        tau = self.step1['tau'].copy()
        tau_filtered = signal.filtfilt(B,A,tau)

        tau_filt_der = np.diff(tau_filtered) / (1.0/ts)
        settling_time = np.max(self.step1['t'])
        max_value = np.max(tau_filt_der)
        for i in range(tau_filt_der.shape[0]-1,0,-1):
            if(np.abs(tau_filt_der[i]) > 0.01*max_value):
                settling_time = self.step1['t'][i]
                break

        if(settling_time == self.step1['t'].max()):
            raise ExecError("The system doesn't have a steady-state value.")

        self.settling_time = settling_time


    def _compute_static_error(self):
        ts = self.ts
        settling_time = self.settling_time

        step1 = self.step1[math.floor(settling_time/ts):]
        step2 = self.step2[math.floor(settling_time/ts):]
        step3 = self.step3[math.floor(settling_time/ts):]

        steps = pd.concat([step1,step2,step3])

        err = (steps['ref'] - steps['tau']).abs()
        self.performance_indicators['static_error'] = err.sum() / len(err)


    def _compute_error(self):
        err = (self.table['ref'] - self.table['tau']).abs()
        mean_index = err.sum() / len(err)
        std_index = err.std()
        self.error['mean'] = mean_index
        self.error['std'] = std_index


    def _compute_linearity(self):
        amplitude_step1 = self.parameters['environment_reference_signal']['amplitude_step_1']
        amplitude_step2 = self.parameters['environment_reference_signal']['amplitude_step_2']
        amplitude_step3 = self.parameters['environment_reference_signal']['amplitude_step_3']

        tau1_norm = self.step1['tau'] / amplitude_step1
        tau2_norm = self.step2['tau'] / amplitude_step2
        tau3_norm = self.step3['tau'] / amplitude_step3

        tau1_norm = tau1_norm.to_numpy()
        tau2_norm = tau2_norm.to_numpy()
        tau3_norm = tau3_norm.to_numpy()

        res1 = np.mean(np.abs(tau1_norm - tau2_norm))
        res2 = np.mean(np.abs(tau1_norm - tau3_norm))
        res3 = np.mean(np.abs(tau2_norm - tau3_norm))

        res = 1 - np.mean([res1, res2, res3])
        if res<0:
            res = 0
        self.performance_indicators['linearity'] = res


    def _compute_overshoot(self):
        ts = self.ts
        settling_time = self.settling_time
        steps = [self.step1, self.step2, self.step3]

        res = 0
        for step in steps:
            tau = step['tau']
            max_value = tau[0:math.floor(settling_time/ts)].max()

            steady_value = tau[math.floor(settling_time/ts):].mean()

            if max_value >= steady_value:
                overshoot = 100.0 * ((max_value - steady_value) / steady_value)

                if overshoot > res:
                    res = overshoot

        self.performance_indicators['overshoot'] = res


    def _compute_dynamic_error(self):
        temp_table = self.table.copy()

        if self.performance_indicators['static_error'] is None:
            self._compute_static_error()

        temp_table['tau'] = temp_table['tau'] + self.performance_indicators['static_error']

        err = (temp_table['ref'] - temp_table['tau']).abs()
        mean_index = err.sum() / len(err)
        std_index = err.std()
        self.dynamic_error['mean'] = mean_index
        self.dynamic_error['std'] = std_index


    def _compute_step_analysis(self, cutoff_freq: float = 10):
        ts = self.ts
        fs = 1.0 / ts

        step1 = self.step1.copy()
        res = {
            "settling_time": (step1['t']).max(),
            "regime_value": 0,
            "delay_time": 0,
            "rising_time": 0,
            "bandwidth": 0
        }

        # low pass filter 2-nd order
        [B,A] = signal.butter(2,cutoff_freq/(fs/2.0))

        tau = step1['tau']
        tau_filtered = signal.lfilter(B,A,tau) # maybe better filtfilt()
        tau_filt_der = np.diff(tau_filtered) / fs
        max_value = np.max(tau_filt_der)

        for i in range(tau_filt_der.shape[0]-1,0,-1):
            if(np.abs(tau_filt_der[i]) > 0.01*max_value):
                res["settling_time"] = step1['t'][i]
                break

        if(res["settling_time"] == (step1['t']).max()):
            raise ExecError("The system doesn't have a steady-state value.")

        # steady output value with time that tends to infinity
        res["regime_value"] = tau_filtered[-1]

        t_index = len(tau) - 1
        for i in range(len(tau)):
            if tau[i] >= 0.5*res["regime_value"]:
                t_index = i
                break

        # time needed to the output to reach the 0.5*regime_value
        res["delay_time"] = step1['t'][t_index]

        # time where output is for the first time at 90% of the regime_value
        t_90 = len(tau) - 1
        for i in range(len(tau)):
            if tau[i] >= 0.9*res["regime_value"]:
                t_90 = i
                break
        t_90 = step1['t'][t_90]

        # time where output is for the first time at 10% of the regime_value
        t_10 = len(tau) - 1
        for i in range(len(tau)):
            if tau[i] >= 0.1*res["regime_value"]:
                t_10 = i
                break
        t_10 = step1['t'][t_10]

        # time needed to the output to go from 10% to 90% of the regime_value, in seconds
        res["rising_time"] = t_90 - t_10
        # the bandwidth in Hz
        res["bandwidth"] = 0.35 / res["rising_time"]

        self.step_analysis = res
        self.performance_indicators['bandwidth'] = res["bandwidth"]


    def _compute_highfidelity_bandwidth(self):
        sweep_max_freq = self.parameters["environment_reference_signal"]["sweep_max_freq"]
        sweep_duration = self.parameters["environment_reference_signal"]["sweep_duration"]
        sweep_offset = self.parameters["environment_reference_signal"]["amplitude_step_4"]

        if self.performance_indicators['bandwidth'] is None:
            self._compute_step_analysis()
        bandwidth = self.performance_indicators["bandwidth"]

        if self.performance_indicators['static_error'] is None:
            self._compute_static_error()
        static_error = self.performance_indicators["static_error"]

        # remove the first t from all the times (so it starts with 0)
        temp_sweep = self.sweep.copy()
        temp_sweep['t'] = temp_sweep['t'] - temp_sweep['t'].iloc[0]

        # remove first samples
        start_index = math.floor(len(temp_sweep)*0.1)

        # angular coefficient of the freq rect
        m = sweep_max_freq / sweep_duration

        temp_sweep['tau'] = (temp_sweep['tau'] + static_error - sweep_offset).abs()
        temp_sweep['ref'] = (temp_sweep['ref'] - sweep_offset).abs()

        err = ((temp_sweep['ref'] - temp_sweep['tau']) / sweep_offset).abs()

        highfidelity_bandwidth = sweep_max_freq
        for i in range(start_index,len(err)):
            if err[i] > 0.293 and start_index + i < len(err):
                highfidelity_bandwidth = temp_sweep['t'][start_index + i]

        self.performance_indicators['highfidelity_bandwidth'] = highfidelity_bandwidth


if __name__ == "__main__":
    f = open('example/experiments_set_A.json',"r")
    parameters = json.load(f)
    f.close()
    e = Experiment(filename="example/example,ke=1,je=0.001,tsettling=0.353end.csv",
        parameters=parameters,ke=0.01,je=0.01)
    print(e.performance_indicators)