from math import e
import os
import shutil
from numpy.lib.shape_base import _put_along_axis_dispatcher
import yaml
import json
import io
try:
    to_unicode = unicode
except NameError:
    to_unicode = str


def convertForecastYaml(experiment_set_name: str,
                        yaml_filepath: str,
                        output_filepath: str) -> None:
    parameters = {
        "type": "evaluation",
        "simulation": {
            "physics_period": None
        },
        "motor": {
            "type": None,
            "jm": None,
            "dm": None,
            "k_spring": None,
            "current_saturation": None
        },
        "motor_controller": {
            "name": None,
            "kp": None,
            "kd": None
        },
        "environment": {
            "damping_factor": None,
            "ke": [],
            "je": []
        },
        "environment_reference_signal": {
            "amplitude_step_1": None,
            "amplitude_step_2": None,
            "amplitude_step_3": None,
            "amplitude_step_4": None,
            "step_duration": None,
            "sweep_amplitude": None,
            "sweep_duration": None,
            "sweep_max_freq": None
        },
        "transparency": {
            "enable": None,
            "k_des": None,
            "b_des": None,
            "j_des": None,
            "sweep_amplitude": None,
            "sweep_duration": None,
            "sweep_max_freq": None
        },
        "loop_frequency": None,
        "experiment_name": None
    }
    with open(yaml_filepath, 'r') as stream:
        data_loaded = yaml.safe_load(stream)
        conf_name = list(data_loaded.keys())[0]
        data_loaded = data_loaded[conf_name]

        kes = data_loaded["tracking"]["environment"]["k_e_vect"]
        jes = data_loaded["tracking"]["environment"]["j_e_vect"]

        if not isinstance(kes, list):
            kes = [kes]
        if not isinstance(jes, list):
            jes = [jes]

        # simulation
        parameters["simulation"]["physics_period"] = data_loaded["h_physics"]
        # motor
        for p in data_loaded["motor"]:
            parameters["motor"][p] = data_loaded["motor"][p]
        # motor controller
        for p in data_loaded["motor_controller"]:
            parameters["motor_controller"][p] = data_loaded["motor_controller"][p]
        # environment
        parameters["environment"]["damping_factor"] = \
            data_loaded["tracking"]["environment"]["damping_factor"]
        parameters["environment"]["ke"] = kes
        parameters["environment"]["je"] = jes
        # environment reference signal
        for p in data_loaded["tracking"]["reference_signal"]:
            parameters["environment_reference_signal"][p] = \
                data_loaded["tracking"]["reference_signal"][p]
        # transparency
        for p in data_loaded["transparency"]["environment"]:
            parameters["transparency"][p] = \
                data_loaded["transparency"]["environment"][p]
        parameters["transparency"]["enable"] = True if \
                data_loaded["transparency"]["enable_transparency"] == 1 else False
        for p in data_loaded["transparency"]["reference_signal"]:
            parameters["transparency"][p] = \
                data_loaded["transparency"]["reference_signal"][p]
        # loop frequency
        parameters["loop_frequency"] = data_loaded["loop"]["frequency"]
        # experiment name
        parameters["experiment_name"] = experiment_set_name

        # change motor_type to type
        parameters["motor"]["type"] = parameters["motor"].pop("motor_type")

    with io.open(output_filepath, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(parameters,
                      indent=4, sort_keys=False,
                      separators=(',', ': '), ensure_ascii=False)
        outfile.write(to_unicode(str_))

    return [kes, jes, parameters["transparency"]["enable"]]


if __name__ == "__main__":
    sesim_dir = 'test_single_sesim/'
    sesim_name = 'example'
    new_dir = 'test_single_next/'
    new_exp_set_name = 'example'

    if not os.path.exists(new_dir):
        os.mkdir(new_dir)

    [kes, jes, transparency_enable] = convertForecastYaml('example',sesim_dir+sesim_name+'.yaml',new_dir+new_exp_set_name+'.json')
    print(new_dir+new_exp_set_name+'.json created')

    filename = "{name},ke={ke},je={je}end.csv"
    experiments_count = 0
    for ke in kes:
        for je in jes:
            f = filename.format(name=sesim_name,ke=ke,je=je)
            if os.path.exists(sesim_dir+f):
                f_path = new_dir+new_exp_set_name+str(experiments_count)+".csv"
                shutil.copy(sesim_dir+f, f_path)
                print(f_path,"created")
            else:
                raise FileNotFoundError('\033[91m'+f+" not found!"+'\033[0m')
            experiments_count += 1
    if transparency_enable:
        filename_off = "{name},transparency=without_control.csv".format(name=sesim_name)
        filename_on = "{name},transparency=with_control.csv".format(name=sesim_name)
        if os.path.exists(sesim_dir+filename_off):
            f_path = new_dir+new_exp_set_name+str(experiments_count)+".csv"
            shutil.copy(sesim_dir+filename_off, f_path)
            experiments_count += 1
            print(f_path,"created")
        else:
            raise FileNotFoundError('\033[91m'+filename_off+" not found!"+'\033[0m')
        if os.path.exists(sesim_dir+filename_on):
            f_path = new_dir+new_exp_set_name+str(experiments_count)+".csv"
            shutil.copy(sesim_dir+filename_on, f_path)
            experiments_count += 1
            print(f_path,"created")
        else:
            raise FileNotFoundError('\033[91m'+filename_on+" not found!"+'\033[0m')
    print('\033[92m'+'\033[1m'+"Completed!"+'\033[0m'+'\033[0m')

