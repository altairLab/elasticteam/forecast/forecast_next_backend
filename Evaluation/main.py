import json
import yaml
import logging
import sys
import glob
import argparse

from ExperimentsSet import *

conversion_table = {
    "linearity": 'linearity',
    "static_error": 'static_error',
    "overshoot": 'overshoot_level',
    "dynamic_error": 'dynamic_error',
    "highfidelity_bandwidth": 'high_fidelity_bandwidth',
    "bandwidth": 'bandwidth',
    "environment_cases": ['best_case_environments', 'worst_case_environments'],
    "transparency": 'transparency',
}

def remove_nan(data: list) -> list:
    try:
        for i in range(len(data)):
            if(math.isnan(data[i])):
                data[i] = 0
    except TypeError:
        pass
    return data

def make_yaml(filename: str, data: dict):
    data["value"] = remove_nan(data["value"])

    with open(filename, 'w') as outfile:
        yaml.dump(data, outfile, default_flow_style=False)

def save_yamls(pis: dict, pi_output_dict: str):
    # linearity
    pi = "linearity"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'scalar',
        value = float(pis[pi]["mean"])
    ))

    # static_error
    pi = "static_error"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'vector',
        label = ['mean', 'std'],
        value = [float(pis[pi]["mean"]), float(pis[pi]["std"])]
    ))

    # overshoot
    pi = "overshoot"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'vector',
        label = ['mean', 'std', 'max'],
        value = [float(pis[pi]["mean"]), float(pis[pi]["std"]), float(pis[pi]["max"])]
    ))

    # dynamic_error
    pi = "dynamic_error"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'vector',
        label = ['mean', 'std'],
        value = [float(pis[pi]["mean"]), float(pis[pi]["std"])]
    ))

    # highfidelity_bandwidth
    pi = "highfidelity_bandwidth"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'vector',
        label = ['mean', 'std', 'min'],
        value = [float(pis[pi]["mean"]), float(pis[pi]["std"]), float(pis[pi]["min"])]
    ))

    # bandwidth
    pi = "bandwidth"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'vector',
        label = ['mean', 'std', 'min'],
        value = [float(pis[pi]["mean"]), float(pis[pi]["std"]), float(pis[pi]["min"])]
    ))

    # environment_cases
    pi = "environment_cases"
    make_yaml(pi_output_dict + 'best_case_environments.yaml', dict(
        type = 'string',
        value = pis[pi]["bce"]
    ))
    make_yaml(pi_output_dict + 'worst_case_environments.yaml', dict(
        type = 'string',
        value = pis[pi]["wce"]
    ))

    # transparency (try - catch because transparency is an optional PI)
    try:
        pi = "transparency"
        make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
            type = 'vector',
            label = pis[pi]["labels"],
            value = pis[pi]["values"]
        ))
    except Exception as e:
        print(e)


def plot_results(performance_indicators: dict, directory_name: str, plot: bool):
    for p in performance_indicators:
        print(p)
        print(performance_indicators[p])
        if(performance_indicators[p]["values"] is not None):
            plt.figure()
            if p != "transparency":
                df = pd.DataFrame.from_dict(json.loads(performance_indicators[p]["values"]))
                plt.pcolor(df) #,cmap=cm.Reds)
                plt.yticks(np.arange(0.5, len(df.index), 1), df.index)
                plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns)
                plt.title(p)
                if p != "environment_cases":
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p]+".png")
                else:
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p][0]+".png")
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p][1]+".png")

                if(plot):
                    plt.show()

                # setup the figure and axes
                fig = plt.figure(figsize=(8, 3))
                ax1 = fig.add_subplot(111, projection='3d')
                _x = np.arange(len(df))
                _y = np.arange(len(df.columns))
                _xx, _yy = np.meshgrid(_x, _y)
                x, y = _xx.ravel(), _yy.ravel()
                top = df.to_numpy().ravel()
                bottom = np.zeros_like(top)
                width = depth = 1

                dz = top
                offset = dz + np.abs(dz.min())
                fracs = offset.astype(float)/offset.max()
                norm = matplotlib.colors.Normalize(fracs.min(), fracs.max())
                colors = matplotlib.cm.jet(norm(fracs))
                ax1.bar3d(x, y, bottom, width, depth, top, shade=True, color=colors)
                # ax1.view_init(elev=90,azim=0)

                if p != "environment_cases":
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p]+"_3d.png")
                else:
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p][0]+"_3d.png")
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p][1]+"_3d.png")

                plt.close(fig=fig)
            else:
                df = (performance_indicators[p]["values"])
                plt.bar(range(0,len(df)),df)
                plt.title(p)
                plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p]+".png")
                if(plot):
                    plt.show()


def compute_pi(directory_name: str, experiments_set_name: str, plot: bool):
    print("Simulation dir:",directory_name)
    print("experiments_set_name:",experiments_set_name)
    print("Plot? " + str(plot))

    try:
        e = ExperimentsSet(str(experiments_set_name),str(directory_name))
        plot_results(e.performance_indicators,directory_name,plot)
        save_yamls(e.performance_indicators,directory_name + 'pi_output/')
    except ValueError as e:
        logging.error("Value error: " + str(e))
    except FileNotFoundError as e:
        logging.error("File error: " + str(e))
    except:
        logging.error("ERROR IN 'compute_pi'.",sys.exc_info()[0])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--plot", help="show the plots",
                        action="store_true")
    parser.add_argument("directory_name", type=str, help="name of the directory containing the dataset")
    parser.add_argument("experiments_set_name", type=str, help="name of the experiments set parameters file")
    args = parser.parse_args()

    # add the final slash if there isn't
    if not args.directory_name.endswith('/'):
        args.directory_name = args.directory_name + "/"

    # make the ouput directory if there isn't
    try:
        os.mkdir(args.directory_name + 'pi_output/')
    except FileExistsError:
        pass

    # remove the older pi results
    try:
        files = glob.glob(args.directory_name + 'pi_output/*')
        for f in files:
            os.remove(f)
    except Exception as e:
        pass

    compute_pi(directory_name=args.directory_name,
                experiments_set_name=args.experiments_set_name,
                plot=args.plot)

