from Identification import *
import pandas as pd
import numpy as np


class MotorInertiaIdentification(Identification):
    required_names = ['t', 'dthetaM', 'ddthetaM', 'tauSensor']

    def __init__(self, filename: str = "", table = None):
        super(MotorInertiaIdentification,self).__init__(filename,table)


    @staticmethod
    def get_required_names():
        return __class__.required_names


    def _load_table(self):
        table = self.table

        t = table['t'].to_numpy()
        tauSensor = table['tauSensor'].to_numpy()
        dthetaM = table['dthetaM'].to_numpy()
        ddthetaM = table['ddthetaM'].to_numpy()

        y = -tauSensor
        phi = np.array([
            ddthetaM
            # dthetaM,
            # np.sign(dthetaM),
            # np.ones(dthetaM.shape)
        ])

        return [y, phi, t]


    def _set_parameters_identified(self, solution):
        self.parameters_identified["Motor inertia"] = solution[0]


if __name__ == "__main__":
    # TODO not tested yet
    print(MotorInertiaIdentification.get_required_names())
    id = MotorInertiaIdentification('motor_inertia.csv')
    id.compute()
    print(id.parameters_identified)
    print(id.res_solution,id.res_sigma)