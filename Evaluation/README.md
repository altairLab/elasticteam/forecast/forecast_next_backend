# Forecast NEXT Evaluation

<!-- [![license - apache 2.0](https://img.shields.io/:license-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) -->

## Purposes

Benchmarking a force control algorithm within different types of environment. It is part of **Forecast NEXT**.

<br>

## Installation

Simply clone this repository. Remember that in order to work it needs **Forecast NEXT** datasets.

<br>

## Usage

The script `main.py` computes the evaluation of performance indicators on a given dataset. The dataset must be compliant with the Forecast NEXT framework (e.g. simulated data from **Forecast NEXT Simulation**, or real testbed data from **Forecast NEXT Nucleo**).

To run the performance indicators computation:

```console
python3 main.py [-p] <directory_name> <experiments_set_name>
```

where *directory_name* is the directory containing the dataset, *experiments_set_name* is the name of the .json file containing the parameters, and *-p* (or *--plot*) indicates to show the plots.

<br>

## Example

An example is provided in the `simulation_output_example/` directory. It contains the simulation input and output data, as well as the input and output data of the PIs computation.

<br>

## Results

The output of the simulations will be found in `'simulation_dir'` directory, while the results of the performance indicator computations will be found in `'simulation_dir'/pi_output/`.

Results of PIs are plots (in .png files) and datas (in .yaml files).

<br>

## Acknowledgements

<a href="http://eurobench2020.eu">
  <img src="http://eurobench2020.eu/wp-content/uploads/2018/06/cropped-logoweb.png"
       alt="rosin_logo" height="60" >
</a>

Supported by Eurobench - the European robotic platform for bipedal locomotion benchmarking.
More information: [Eurobench website][eurobench_website]

<img src="http://eurobench2020.eu/wp-content/uploads/2018/02/euflag.png"
     alt="eu_flag" width="100" align="left" >

This project has received funding from the European Union’s Horizon 2020
research and innovation programme under grant agreement no. 779963.

The opinions and arguments expressed reflect only the author‘s view and
reflect in no way the European Commission‘s opinions.
The European Commission is not responsible for any use that may be made
of the information it contains.

[eurobench_logo]: http://eurobench2020.eu/wp-content/uploads/2018/06/cropped-logoweb.png
[eurobench_website]: http://eurobench2020.eu "Go to website"
