import json
import yaml
import glob
import math
import websockets
import asyncio
import logging
import sys

from ExperimentsSet import *


# Remove the NaN from a dict, replacing them with None
def remove_NaN(d: dict) -> dict:
    for k in d:
        if isinstance(d[k], dict):
            d[k] = remove_NaN(d[k])
        elif isinstance(d[k], float) and math.isnan(d[k]):
            d[k] = None
    return d


conversion_table = {
    "linearity": 'linearity',
    "static_error": 'static_error',
    "overshoot": 'overshoot_level',
    "dynamic_error": 'dynamic_error',
    "highfidelity_bandwidth": 'high_fidelity_bandwidth',
    "bandwidth": 'bandwidth',
    "environment_cases": ['best_case_environments', 'worst_case_environments'],
    "transparency": 'transparency',
}

def remove_none(data: list) -> list:
    try:
        for i in range(len(data)):
            if(data[i] == None):
                data[i] = 0
    except TypeError:
        pass
    return data

def make_yaml(filename: str, data: dict):
    data["value"] = remove_none(data["value"])

    with open(filename, 'w') as outfile:
        yaml.dump(data, outfile, default_flow_style=False)

def save_yamls(pis: dict, pi_output_dict: str):
    # linearity
    pi = "linearity"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'scalar',
        value = float(pis[pi]["mean"] or 0)
    ))

    # static_error
    pi = "static_error"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'vector',
        label = ['mean', 'std'],
        value = [float(pis[pi]["mean"] or 0), float(pis[pi]["std"] or 0)]
    ))

    # overshoot
    pi = "overshoot"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'vector',
        label = ['mean', 'std', 'max'],
        value = [float(pis[pi]["mean"] or 0), float(pis[pi]["std"] or 0), float(pis[pi]["max"] or 0)]
    ))

    # dynamic_error
    pi = "dynamic_error"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'vector',
        label = ['mean', 'std'],
        value = [float(pis[pi]["mean"] or 0), float(pis[pi]["std"] or 0)]
    ))

    # highfidelity_bandwidth
    pi = "highfidelity_bandwidth"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'vector',
        label = ['mean', 'std', 'min'],
        value = [float(pis[pi]["mean"] or 0), float(pis[pi]["std"] or 0), float(pis[pi]["min"] or 0)]
    ))

    # bandwidth
    pi = "bandwidth"
    make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
        type = 'vector',
        label = ['mean', 'std', 'min'],
        value = [float(pis[pi]["mean"] or 0), float(pis[pi]["std"] or 0), float(pis[pi]["min"] or 0)]
    ))

    # environment_cases
    pi = "environment_cases"
    make_yaml(pi_output_dict + 'best_case_environments.yaml', dict(
        type = 'string',
        value = pis[pi]["bce"]
    ))
    make_yaml(pi_output_dict + 'worst_case_environments.yaml', dict(
        type = 'string',
        value = pis[pi]["wce"]
    ))

    # transparency (try - catch because transparency is an optional PI)
    try:
        pi = "transparency"
        make_yaml(pi_output_dict + conversion_table[pi] + '.yaml', dict(
            type = 'vector',
            label = pis[pi]["labels"],
            value = pis[pi]["values"]
        ))
    except Exception as e:
        print(e)


def save_plots(performance_indicators: dict, directory_name: str):
    for p in performance_indicators:
        if(performance_indicators[p]["values"] is not None):
            plt.figure()
            if p != "transparency":
                df = pd.DataFrame.from_dict(json.loads(performance_indicators[p]["values"]))
                plt.pcolor(df) #,cmap=cm.Reds)
                plt.yticks(np.arange(0.5, len(df.index), 1), df.index)
                plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns)
                plt.title(p)
                if p != "environment_cases":
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p]+".png")
                else:
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p][0]+".png")
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p][1]+".png")

                # setup the figure and axes
                fig = plt.figure(figsize=(8, 3))
                ax1 = fig.add_subplot(111, projection='3d')
                _x = np.arange(len(df))
                _y = np.arange(len(df.columns))
                _xx, _yy = np.meshgrid(_x, _y)
                x, y = _xx.ravel(), _yy.ravel()
                top = df.to_numpy().ravel()
                bottom = np.zeros_like(top)
                width = depth = 1

                dz = top
                offset = dz + np.abs(dz.min())
                fracs = offset.astype(float)/offset.max()
                norm = matplotlib.colors.Normalize(fracs.min(), fracs.max())
                colors = matplotlib.cm.jet(norm(fracs))
                ax1.bar3d(x, y, bottom, width, depth, top, shade=True, color=colors)
                # ax1.view_init(elev=90,azim=0)

                if p != "environment_cases":
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p]+"_3d.png")
                else:
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p][0]+"_3d.png")
                    plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p][1]+"_3d.png")

                plt.close(fig=fig)
            else:
                df = (performance_indicators[p]["values"])
                plt.bar(range(0,len(df)),df)
                plt.title(p)
                plt.savefig(fname=directory_name+"pi_output/"+conversion_table[p]+".png")
            plt.close()


# Websocket server waiting for messages to compute the PIs.
# The structure of the incoming messages must be:
# command: "compute_pi"
# dir: directory of the dataset
# name: name of the experiments set
async def compute_pi(websocket, path):
    async for message in websocket:
        data = json.loads(message)
        if "command" not in data:
            err_str = "Unsupported packet: " + str(data)
            logging.error(err_str)
            await websocket.send(json.dumps({"error": err_str}))
        if data["command"] == "compute_pi":
            if "dir" not in data or "name" not in data:
                err_str = "Unsupported event: " + str(data)
                logging.error(err_str)
                await websocket.send(json.dumps({"error": err_str}))
            else:
                directory_name = data["dir"]
                experiments_set_name = data["name"]
                print("Simulation dir:",directory_name)
                print("experiments_set_name:",experiments_set_name)

                # add the final slash if there isn't
                if not directory_name.endswith('/'):
                    directory_name = directory_name + "/"

                # make the ouput directory if there isn't
                try:
                    os.mkdir(directory_name + 'pi_output/')
                except FileExistsError:
                    pass

                # remove the older pi results
                try:
                    files = glob.glob(directory_name + 'pi_output/*')
                    for f in files:
                        os.remove(f)
                except Exception as e:
                    pass

                try:
                    e = ExperimentsSet(str(experiments_set_name),str(directory_name))
                    d = remove_NaN(e.performance_indicators)
                    save_plots(e.performance_indicators,directory_name)
                    save_yamls(e.performance_indicators,directory_name + 'pi_output/')
                    print("RESULT:",json.dumps(d))
                    await websocket.send(json.dumps(d))
                except ValueError as e:
                    logging.error("Value error")
                    await websocket.send(json.dumps({"error": str(e)}))
                except FileNotFoundError as e:
                    logging.error("File error")
                    await websocket.send(json.dumps({"error": str(e)}))
                except:
                    logging.error("ERROR IN 'compute_pi'.",sys.exc_info()[0])
        else:
            err_str = "Unsupported command: " + str(data["command"])
            logging.error(err_str)
            await websocket.send(json.dumps({"error": err_str}))


if __name__ == "__main__":
    logging.basicConfig()
    start_server = websockets.serve(compute_pi, "localhost", 6790)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()

