import matplotlib
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os

import json
import io
try:
    to_unicode = unicode
except NameError:
    to_unicode = str

from Experiment import *
from TransparencyExperiment import *

class ExperimentsSet:
    # json file containing the parameters of this set of experiment
    filename_parameters = ""
    # name of the directory containing all the files
    directory_name = ""

    # parameters dict
    parameters = {}

    # performance indicators
    performance_indicators = {
        "linearity": {
            "values": None,
            "mean": 0,
            "std": 0
        },
        "static_error": {
            "values": None,
            "mean": 0,
            "std": 0
        },
        "overshoot": {
            "values": None,
            "mean": 0,
            "std": 0
        },
        "dynamic_error": {
            "values": None,
            "mean": 0,
            "std": 0
        },
        "highfidelity_bandwidth": {
            "values": None,
            "mean": 0,
            "std": 0
        },
        "bandwidth": {
            "values": None,
            "mean": 0,
            "std": 0
        },
        "environment_cases": {
            "values": None,
            "bce": "",
            "wce": ""
        },
        "transparency": {
            "values": None,
            "mean": 0,
            "std": 0
        }
    }

    # list of experiments
    experiments = []


    def __init__(self, filename_parameters: str, directory_name: str) -> None:
        self.filename_parameters = filename_parameters
        if(directory_name.endswith("/") == False):
            directory_name += "/"
        self.directory_name = directory_name
        self._read_parameters()

        # check if there is "type" set to "evaluation" in the parameters
        if "type" not in self.parameters or \
            self.parameters["type"] != "evaluation":
            raise FileNotFoundError("ERROR: File not supported")

        # compute the performance indicators
        self._compute_performance_indicators()


    def _compute_environment_cases(self, errors_mean):
        kes = self.parameters["environment"]["ke"]
        jes = self.parameters["environment"]["je"]
        kes_l = len(set(kes))
        jes_l = len(set(jes))

        res_values = pd.DataFrame()
        for ke in ['LowKe', 'HighKe']:
            res_values.insert(len(res_values.columns),str(ke),np.zeros(len(res_values)))
        for je in ['LowJe', 'HighJe']:
            row = pd.DataFrame([np.zeros(len(res_values.columns))], columns=list(res_values.columns), index=[str(je)])
            res_values = res_values.append(row)

        res_values_je_index = 0
        res_values_ke_index = 0
        for ke_i in range(0,kes_l):
            if ke_i < math.floor(kes_l/2):
                res_values_ke_index = 0
            elif kes_l%2 == 0 or math.floor(kes_l/2)!=ke_i:
                res_values_ke_index = 1
            else:
                continue
            for je_i in range(0,jes_l):
                if je_i < math.floor(jes_l/2):
                    res_values_je_index = 0
                elif jes_l%2 == 0 or math.floor(jes_l/2)!=je_i:
                    res_values_je_index = 1
                else:
                    continue

                res_values.iloc[res_values_je_index].iloc[res_values_ke_index] += errors_mean.iloc[je_i].iloc[ke_i]

        max_item = res_values.iloc[0].iloc[0]
        max_index = [0, 0]
        min_item = res_values.iloc[0].iloc[0]
        min_index = [0, 0]

        for i in range(0,2):
            for j in range(0,2):
                if res_values.iloc[i].iloc[j] > max_item:
                    max_item = res_values.iloc[i].iloc[j]
                    max_index = [j, i]
                if res_values.iloc[i].iloc[j] < min_item:
                    min_item = res_values.iloc[i].iloc[j]
                    min_index = [j, i]

        labels = ["Low", "High"]
        worst = labels[max_index[0]] + "Ke" + labels[max_index[1]] + "Je"
        best = labels[min_index[0]] + "Ke" + labels[min_index[1]] + "Je"
        self.performance_indicators["environment_cases"]["values"] = res_values.to_json()
        self.performance_indicators["environment_cases"]["bce"] = best
        self.performance_indicators["environment_cases"]["wce"] = worst
        self.performance_indicators["environment_cases"]["best_value"] = 0
        self.performance_indicators["environment_cases"]["worst_value"] = 10
        self.performance_indicators["environment_cases"]["chart_type"] = "heatmap"


    def _compute_performance_indicators(self):
        kes = self.parameters["environment"]["ke"]
        jes = self.parameters["environment"]["je"]
        number_of_exps = len(kes)*len(jes)

        errors_mean = pd.DataFrame()

        for ke in list(set(kes)):
            errors_mean.insert(len(errors_mean.columns),str(ke),np.zeros(len(errors_mean)))

        for je in list(set(jes)):
            row = pd.DataFrame([np.zeros(len(errors_mean.columns))], columns=list(errors_mean.columns), index=[str(je)])
            errors_mean = errors_mean.append(row)

        errors_std = errors_mean.copy()
        dynamic_error_mean = errors_mean.copy()
        dynamic_error_std = errors_mean.copy()
        linearity_indexes = errors_mean.copy()
        static_error_indexes = errors_mean.copy()
        overshoot_indexes = errors_mean.copy()
        highfidelity_bandwidth_indexes = errors_mean.copy()
        bandwidth_indexes = errors_mean.copy()

        experiment_index = 0
        for ke in kes:
            for je in jes:
                ke = str(ke)
                je = str(je)

                experiment = Experiment(filename=self.directory_name +
                    self.parameters["experiment_name"] +
                    str(experiment_index) + ".csv",
                    parameters=self.parameters, ke=ke, je=je)

                self.experiments.append(experiment)

                errors_mean[ke][je] = experiment.error["mean"]
                errors_std[ke][je] = experiment.error["std"]
                dynamic_error_mean[ke][je] = experiment.dynamic_error["mean"]
                dynamic_error_std[ke][je] = experiment.dynamic_error["std"]
                linearity_indexes[ke][je] = experiment.performance_indicators["linearity"]
                static_error_indexes[ke][je] = experiment.performance_indicators["static_error"]
                overshoot_indexes[ke][je] = experiment.performance_indicators["overshoot"]
                highfidelity_bandwidth_indexes[ke][je] = experiment.performance_indicators["highfidelity_bandwidth"]
                bandwidth_indexes[ke][je] = experiment.performance_indicators["bandwidth"]

                experiment_index += 1

        self.performance_indicators["linearity"]["values"] = linearity_indexes.to_json()
        self.performance_indicators["linearity"]["mean"] = linearity_indexes.mean(axis="columns").mean(axis="index")
        self.performance_indicators["linearity"]["std"] = linearity_indexes.std(axis="columns").std(axis="index")
        self.performance_indicators["linearity"]["best_value"] = 1
        self.performance_indicators["linearity"]["worst_value"] = 0
        self.performance_indicators["linearity"]["chart_type"] = "heatmap"

        self.performance_indicators["static_error"]["values"] = static_error_indexes.to_json()
        self.performance_indicators["static_error"]["mean"] = static_error_indexes.mean(axis="columns").mean(axis="index")
        self.performance_indicators["static_error"]["std"] = static_error_indexes.std(axis="columns").std(axis="index")
        self.performance_indicators["static_error"]["best_value"] = 0
        self.performance_indicators["static_error"]["worst_value"] = 10
        self.performance_indicators["static_error"]["chart_type"] = "heatmap"

        self.performance_indicators["overshoot"]["values"] = overshoot_indexes.to_json()
        self.performance_indicators["overshoot"]["mean"] = overshoot_indexes.mean(axis="columns").mean(axis="index")
        self.performance_indicators["overshoot"]["std"] = overshoot_indexes.std(axis="columns").std(axis="index")
        self.performance_indicators["overshoot"]["max"] = overshoot_indexes.max(axis="columns").max(axis="index")
        self.performance_indicators["overshoot"]["best_value"] = 0
        self.performance_indicators["overshoot"]["worst_value"] = 100
        self.performance_indicators["overshoot"]["chart_type"] = "heatmap"

        self.performance_indicators["dynamic_error"]["values"] = dynamic_error_mean.to_json()
        self.performance_indicators["dynamic_error"]["mean"] = dynamic_error_mean.mean(axis="columns").mean(axis="index")
        self.performance_indicators["dynamic_error"]["std"] = dynamic_error_mean.std(axis="columns").std(axis="index")
        self.performance_indicators["dynamic_error"]["best_value"] = 0
        self.performance_indicators["dynamic_error"]["worst_value"] = 10
        self.performance_indicators["dynamic_error"]["chart_type"] = "heatmap"

        self.performance_indicators["highfidelity_bandwidth"]["values"] = highfidelity_bandwidth_indexes.to_json()
        self.performance_indicators["highfidelity_bandwidth"]["mean"] = highfidelity_bandwidth_indexes.mean(axis="columns").mean(axis="index")
        self.performance_indicators["highfidelity_bandwidth"]["std"] = highfidelity_bandwidth_indexes.std(axis="columns").std(axis="index")
        self.performance_indicators["highfidelity_bandwidth"]["min"] = highfidelity_bandwidth_indexes.min(axis="columns").min(axis="index")
        self.performance_indicators["highfidelity_bandwidth"]["best_value"] = 10
        self.performance_indicators["highfidelity_bandwidth"]["worst_value"] = 0
        self.performance_indicators["highfidelity_bandwidth"]["chart_type"] = "heatmap"

        self.performance_indicators["bandwidth"]["values"] = bandwidth_indexes.to_json()
        self.performance_indicators["bandwidth"]["mean"] = bandwidth_indexes.mean(axis="columns").mean(axis="index")
        self.performance_indicators["bandwidth"]["std"] = bandwidth_indexes.std(axis="columns").std(axis="index")
        self.performance_indicators["bandwidth"]["min"] = bandwidth_indexes.min(axis="columns").min(axis="index")
        self.performance_indicators["bandwidth"]["best_value"] = 20
        self.performance_indicators["bandwidth"]["worst_value"] = 0
        self.performance_indicators["bandwidth"]["chart_type"] = "heatmap"

        self._compute_environment_cases(errors_mean=errors_mean)

        # check if the transparency is enable
        do_transparency = False
        try:
            if self.parameters["transparency"]["enable"] == True:
                do_transparency = True
        except KeyError:
            print("No transparency")

        if do_transparency == True:
            experiment_off_filename = self.directory_name + \
                self.parameters["experiment_name"] + \
                str(experiment_index) + ".csv"
            experiment_index += 1
            experiment_on_filename = self.directory_name + \
                self.parameters["experiment_name"] + \
                str(experiment_index) + ".csv"
            tr = TransparencyExperiment(parameters=self.parameters,
                    experiment_off_filename=experiment_off_filename,
                    experiment_on_filename=experiment_on_filename)

            tr_res = tr.results
            tr_labels = tr.labels
            self.performance_indicators["transparency"]["values"] = tr_res
            self.performance_indicators["transparency"]["labels"] = tr_labels
            self.performance_indicators["transparency"]["mean"] = np.mean(tr_res)
            self.performance_indicators["transparency"]["std"] = np.std(tr_res)
            self.performance_indicators["transparency"]["best_value"] = 10
            self.performance_indicators["transparency"]["worst_value"] = 0
            self.performance_indicators["transparency"]["chart_type"] = "barplot"


    def _read_parameters(self):
        f = open(self.directory_name + self.filename_parameters,"r")
        self.parameters = json.load(f)
        f.close()


if __name__ == "__main__":
    set_dir = 'simulation_outputs/'
    e = ExperimentsSet('example.json',set_dir)

    with io.open(set_dir + 'results.json', 'w', encoding='utf8') as outfile:
        str_ = json.dumps(e.performance_indicators,
                    indent=4, sort_keys=False,
                    separators=(',', ': '), ensure_ascii=False)
        outfile.write(to_unicode(str_))
        print(str_)

    try:
        os.mkdir(set_dir+'pi_output/')
    except FileExistsError:
        pass

    for p in e.performance_indicators:
        print(p)
        print(e.performance_indicators[p])
        if(e.performance_indicators[p]["values"] is not None):
            plt.figure()
            if p != "transparency":
                df = pd.DataFrame.from_dict(json.loads(e.performance_indicators[p]["values"]))
                plt.pcolor(df) #,cmap=cm.Reds)
                plt.yticks(np.arange(0.5, len(df.index), 1), df.index)
                plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns)
                plt.title(p)
                plt.savefig(fname=set_dir+"pi_output/"+p+".png")

                # setup the figure and axes
                fig = plt.figure(figsize=(8, 3))
                ax1 = fig.add_subplot(111, projection='3d')
                # fake data
                _x = np.arange(len(df))
                _y = np.arange(len(df.columns))
                _xx, _yy = np.meshgrid(_x, _y)
                x, y = _xx.ravel(), _yy.ravel()
                top = df.to_numpy().ravel()
                bottom = np.zeros_like(top)
                width = depth = 1

                dz = top
                offset = dz + np.abs(dz.min())
                fracs = offset.astype(float)/offset.max()
                norm = matplotlib.colors.Normalize(fracs.min(), fracs.max())
                colors = matplotlib.cm.jet(norm(fracs))
                ax1.bar3d(x, y, bottom, width, depth, top, shade=True, color=colors)
                # ax1.view_init(elev=90,azim=0)

                # plt.show()
                plt.savefig(fname=set_dir+"pi_output/"+p+"2.png")
            else:
                df = (e.performance_indicators[p]["values"])
                plt.bar(range(0,len(df)),df)
                plt.title(p)
                plt.savefig(fname=set_dir+"pi_output/"+p+".png")
            # plt.show()