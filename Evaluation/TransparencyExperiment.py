from shutil import ExecError
from typing import List
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

from pathlib import Path
import shutil
import json

import math
import pandas as pd
from Experiment import Experiment
from MotorInertiaIdentification import MotorInertiaIdentification

class TransparencyExperiment:
    # csv files containing the data of the two experiments
    filenames = []

    # parameters dictionaries of the two experiments
    parameters = []

    # tables of the two experiments
    tables = []

    # results of the computation
    results = []

    # labels
    labels = []

    def __init__(self, parameters: dict,
                experiment_off_filename: int,
                experiment_on_filename: int) -> None:
        self.parameters = self._get_experiment_parameters(parameters=parameters)
        self.filenames = [experiment_off_filename, experiment_on_filename]

        # check if the transparency is enable
        try:
            if self.parameters["transparency"]["enable"] == False:
                raise KeyError
        except KeyError:
            raise ExecError("File not supported for transparency PI")

        self._read_csvs()
        self._compute_transparency()


    def _get_experiment_parameters(self, parameters: dict):
        exp_params = {}
        for p in parameters:
            if type(parameters[p]) == dict:
                exp_params[p] = self._get_experiment_parameters(parameters[p])
            else:
                exp_params[p] = parameters[p]
        return exp_params


    def _read_csvs(self):
        for i in range(0,len(self.filenames)):
            # read the table
            table = pd.read_csv(self.filenames[i])

            # rename the column 'time' to 't' (if present)
            table.rename({'time': 't'},inplace=True,axis='columns')

            # remove the leading and trailing spaces in the columns names
            for x in list(table.columns):
                table.rename({x: x.strip()},inplace=True,axis="columns")

            # the required names for the evaluation
            required_names = ['t', 'ref', 'tau', 'thetaM',
                            'dthetaM', 'ddthetaM', 'tauSensor',
                            'tauM', 'thetaE']

            # remove the non-necessary columns
            table = table[table.columns.intersection(required_names)]

            # check if the remaining columns are the required ones
            if len(required_names) != len(table.columns):
                raise ExecError("Missing parameters " + str(list(table.columns))
                    + ", required " + str(required_names))

            # remove the first t from all the times (so it starts with 0)
            table['t'] = table['t'] - table['t'].iloc[0]
            table = table.reset_index()

            # set the sampling time
            self.ts = table['t'].diff().mean()

            # set the table
            self.tables.append(table)


    def _compute_transparency(self) -> dict:
        inertias = []
        variances = []
        labels = []
        for i in range(0,len(self.tables)):
            e = self.tables[i]
            # table start from time 0
            e['t'] = e['t'] - e['t'].iloc[0]

            # how many Hz consider for each step analysis
            step_analysis_interval = 1

            # reference signal variables
            sweep_max_freq = self.parameters["transparency"]["sweep_max_freq"]
            duration = self.parameters["transparency"]["sweep_duration"]

            t = e['t'].copy()
            dthetaM = e['dthetaM'].copy()
            ddthetaM = e['ddthetaM'].copy()
            tauSensor = e['tauSensor'].copy()
            fs = self.parameters["loop_frequency"]

            # number of samples per sub-csv
            samples = math.floor(duration/sweep_max_freq * fs * step_analysis_interval)

            # # an error while calculating the second derivative of motor movement
            # if math.isinf(ddthetaM.copy().iloc[0]) or math.isnan(ddthetaM.iloc[0]):
            #     ddthetaM.iloc[0] = 0
            ddthetaM.replace([np.inf, -np.inf], np.nan, inplace=True)
            ddthetaM.fillna(0, inplace=True)

            matrix4analysis = []

            # csv split
            for j in range(0,math.floor(len(t)/samples)):
                actual = pd.DataFrame()
                t1 = j*samples
                t2 = (j+1) * samples
                actual = t[t1:t2].reset_index(drop=True).copy()
                actual = pd.concat([actual,dthetaM[t1:t2].reset_index(drop=True)],axis=1)
                actual = pd.concat([actual,ddthetaM[t1:t2].reset_index(drop=True)],axis=1)
                actual = pd.concat([actual,tauSensor[t1:t2].reset_index(drop=True)],axis=1)

                matrix4analysis.append(actual)

            # calculate inertia of each interval
            inertias.append([])
            variances.append([])
            for j in range(0,len(matrix4analysis)):
                actual_table = matrix4analysis[j]
                id = MotorInertiaIdentification(table=actual_table)
                id.compute(confidence=2, max_iterations=1, filter=True,
                    filter_order=8, cutoff_freq=10)
                inertias[i].append(id.res_solution[0])
                variances[i].append(id.res_sigma[0])
            # calculate the 95 percentile
            # sigma = []
            # for j in range(0,len(variances)):
            #     sigma.append([])
            #     sigma[j].append(inertias[i][j]+3*math.sqrt(variances[j]))
            #     sigma[j].append(inertias[i][j]-3*math.sqrt(variances[j]))

            # print(sigma)


            labels = [] # x axis labels
            labelsPos = [] # position of labels

            # plot the experiment result
            for j in range(0,len(matrix4analysis)):
                # plt.plot(matrix4analysis[j]['t'],matrix4analysis[j]['tauSensor'])
                # plt.axvline(matrix4analysis[j]['t'].max())
                labelsPos.append(matrix4analysis[j]['t'].max())
                labels.append(str(j*step_analysis_interval+1) + 'Hz')

            # plt.xticks(labelsPos,labels=labels)
            # plt.xlim([0,labelsPos[-1]])
            # plt.show()

        res = []
        for i in range(len(inertias[0])):
            res.append((inertias[0][i] / inertias[1][i]) -1 )
        self.results = res
        self.labels = labels
