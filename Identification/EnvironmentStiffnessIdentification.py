from Identification import *


class EnvironmentStiffnessIdentification(Identification):
    required_names = ['t', 'thetaE', 'tauSensor']

    def __init__(self, filename: str):
        super(EnvironmentStiffnessIdentification,self).__init__(filename)


    @staticmethod
    def get_required_names():
        return __class__.required_names


    def _load_table(self):
        table = self.table

        t = table['t'].to_numpy()
        tauSensor = table['tauSensor'].to_numpy()
        thetaE = table['thetaE'].to_numpy()

        y = tauSensor
        phi = np.array([
            thetaE,
            np.ones(thetaE.shape),
        ])

        return [y, phi, t]


    def _set_parameters_identified(self, solution):
        self.parameters_identified["environment stiffness"] = solution[0]


if __name__ == "__main__":
    # TODO not tested yet
    id = EnvironmentStiffnessIdentification('/home/rudyvic/Desktop/EMANUELE/EnvironmentIdentification/data/log256.csv')
    id.compute()
    print(id.parameters_identified)
    print(id.res_solution,id.res_sigma,type(id.res_solution),type(id.res_sigma))