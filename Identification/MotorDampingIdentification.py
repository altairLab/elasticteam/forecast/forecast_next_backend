from Identification import *


class MotorDampingIdentification(Identification):
    required_names = ['t', 'thetaM', 'dthetaM', 'tauSensor']

    def __init__(self, filename: str):
        super(MotorDampingIdentification,self).__init__(filename)


    @staticmethod
    def get_required_names():
        return __class__.required_names


    def _load_table(self):
        table = self.table

        t = table['t'].to_numpy()
        tauSensor = table['tauSensor'].to_numpy()
        thetaM = table['thetaM'].to_numpy()
        dthetaM = table['dthetaM'].to_numpy()

        y = tauSensor
        phi = np.array([
            thetaM,
            dthetaM,
            np.ones(dthetaM.shape)
        ])

        return [y, phi, t]


    def _set_parameters_identified(self, solution):
        self.parameters_identified["motor damping"] = solution[1]


if __name__ == "__main__":
    id = MotorDampingIdentification('env_damping.csv')
    # print(id.filename)
    # my_data = read_csv('pose_2.csv')
    id.compute()
    print(id.parameters_identified)
    print(id.res_solution,id.res_sigma,type(id.res_solution),type(id.res_sigma))
    # id = Identification()
    # id._least_square_identification()