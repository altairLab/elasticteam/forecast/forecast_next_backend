from Identification import *


class EnvironmentInertiaIdentification(Identification):
    required_names = ['t', 'thetaE', 'dthetaE', 'ddthetaE', 'tauSensor']

    def __init__(self, filename: str):
        super(EnvironmentInertiaIdentification,self).__init__(filename)


    @staticmethod
    def get_required_names():
        return __class__.required_names


    def _load_table(self):
        table = self.table

        t = table['t'].to_numpy()
        tauSensor = table['tauSensor'].to_numpy()
        thetaE = table['thetaE'].to_numpy()
        dthetaE = table['dthetaE'].to_numpy()
        ddthetaE = table['ddthetaE'].to_numpy()

        y = tauSensor
        phi = np.array([
            thetaE,
            dthetaE,
            ddthetaE,
            np.sign(dthetaE),
            np.ones(thetaE.shape)
        ])

        return [y, phi, t]


    def _set_parameters_identified(self, solution):
        self.parameters_identified["environment inertia"] = solution[2]
        # self.parameters_identified["environment inertia"] = solution[3]


if __name__ == "__main__":
    # TODO not tested yet
    print(EnvironmentInertiaIdentification.get_required_names())
    id = EnvironmentInertiaIdentification('env_inertia.csv')
    id.compute()
    print(id.parameters_identified)
    print(id.res_solution,id.res_sigma)