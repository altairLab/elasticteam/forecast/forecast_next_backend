import json
import math
import websockets
import asyncio
import logging
import sys
from pathlib import Path

from Identification import *
from TestbedFrictionIdentification import *
from MotorFrictionIdentification import *
from EnvironmentStiffnessIdentification import *
from EnvironmentDampingIdentification import *
from EnvironmentInertiaIdentification import *
import IdentificationUtility as iu
from MotorStiffnessIdentification import *
from MotorDampingIdentification import *
from MotorInertiaIdentification import *


# Remove the NaN from a dict, replacing them with None
def remove_NaN(d: dict) -> dict:
    for k in d:
        if isinstance(d[k], dict):
            d[k] = remove_NaN(d[k])
        elif isinstance(d[k], float) and math.isnan(d[k]):
            d[k] = None
    return d


identifications_list = [
    "Motor friction",
    "Environment stiffness",
    "Environment damping",
    "Environment inertia",
    "Motor stiffness",
    "Motor damping",
    "Motor inertia"
]

def remove_none(data: list) -> list:
    try:
        for i in range(len(data)):
            if(data[i] == None):
                data[i] = 0
    except TypeError:
        pass
    return data


# Websocket server waiting for messages to perform the identification task.
# The structure of the incoming messages must be:
# command:
# - "identificate" - perform an identification task
#       id_task: string that identifies the task identification to perform
#       path: path of the .csv to identificate
# - "task_list" - return the list of the available identification tasks
async def identificate(websocket, path):
    async for message in websocket:
        data = json.loads(message)
        if "command" not in data:
            err_str = "Unsupported packet: " + str(data)
            logging.error(err_str)
            await websocket.send(json.dumps({"error": err_str}))
        if data["command"] == "identificate":
            if "id_task" not in data or "path" not in data:
                err_str = "Unsupported event: " + str(data)
                logging.error(err_str)
                await websocket.send(json.dumps({"error": err_str}))
            else:
                id_task = data["id_task"]
                filename = data["path"]
                print("Task identification:",id_task)
                print("File path:",filename)

                try:
                    task = None
                    if(id_task == "Motor friction"):
                        task = MotorFrictionIdentification(filename=filename)
                    elif(id_task == "Environment stiffness"):
                        task = EnvironmentStiffnessIdentification(filename=filename)
                    elif(id_task == "Environment damping"):
                        task = EnvironmentDampingIdentification(filename=filename)
                    elif(id_task == "Environment inertia"):
                        task = EnvironmentInertiaIdentification(filename=filename)
                    elif(id_task == "Motor stiffness"):
                        task = MotorStiffnessIdentification(filename=filename)
                    elif(id_task == "Motor damping"):
                        task = MotorDampingIdentification(filename=filename)
                    elif(id_task == "Motor inertia"):
                        task = MotorInertiaIdentification(filename=filename)

                    task.compute()

                    # d = remove_NaN(json.dumps({"solutions": task.res_solution, "sigma": task.res_sigma}))
                    d = json.dumps({"solutions": task.res_solution, "sigma": task.res_sigma, "identified": task.parameters_identified})
                    print("RESULT:",d)
                    await websocket.send(d)
                except ValueError as e:
                    logging.error("Value error")
                    await websocket.send(json.dumps({"error": str(e)}))
                except FileNotFoundError as e:
                    logging.error("File error")
                    await websocket.send(json.dumps({"error": str(e)}))
                except Exception as e:
                    logging.error("ERROR IN 'identificate'.",sys.exc_info()[0])
                    await websocket.send(json.dumps({"error": str(e)}))
        elif data["command"] == "testbed_friction_identification":
            print("Testbed friction identification")
            err = compute_testbed_friction_identification("/.config/Electron/logs/FrictionIdentification")
            if len(err) != 0:
                logging.error("ERROR IN 'testbed_friction_identification'.",err)
                await websocket.send(json.dumps({"error": str(err)}))
            else:
                try:
                    csv_path = str(Path.home()) + f"/.config/Electron/logs/out_friction_neg.csv"
                    id = TestbedFrictionIdentification(csv_path)
                    id.compute(confidence=10,plot=False)
                    neg_res = id.parameters_identified

                    csv_path = str(Path.home()) + f"/.config/Electron/logs/out_friction_pos.csv"
                    id = TestbedFrictionIdentification(csv_path)
                    id.compute(confidence=10,plot=False)
                    pos_res = id.parameters_identified

                    d = json.dumps({"neg_res": neg_res, "pos_res": pos_res})
                    print("RESULT:",d)
                    await websocket.send(d)
                except Exception as e:
                    logging.error("ERROR IN 'testbed_friction_identification'.",sys.exc_info()[0])
                    await websocket.send(json.dumps({"error": str(e)}))
        elif data["command"] == "task_list":
            print("Task list")
            await websocket.send(json.dumps({"list": identifications_list}))
        elif data["command"] == "get_csv_params":
            if "path" not in data:
                err_str = "Unsupported event: " + str(data)
                logging.error(err_str)
                await websocket.send(json.dumps({"error": err_str}))
            else:
                filename = data["path"]
                params = iu.get_csv_params(filename)
                if len(params) == 0:
                    logging.error("ERROR IN 'get_csv_params'.")
                    await websocket.send(json.dumps({"error": "Can't retrieve the parameters of this file"}))
                else:
                    d = json.dumps({"params": params})
                    print("RESULT:",d)
                    await websocket.send(d)
        elif data["command"] == "change_friction_log_names":
            print("Change friction log names")
            err = iu.change_friction_log_names()
            if len(err) != 0:
                logging.error("ERROR IN 'change_friction_log_names'.",err)
                await websocket.send(json.dumps({"error": str(err)}))
        elif data["command"] == "open_exp_dir":
            print("Open experiment directory")
            err = iu.open_exp_dir()
            if len(err) != 0:
                logging.error("ERROR IN 'open_exp_dir'.",err)
                await websocket.send(json.dumps({"error": str(err)}))
        elif data["command"] == "clear_exp_dir":
            print("Clear experiment directory")
            err = iu.clear_exp_dir()
            if len(err) != 0:
                logging.error("ERROR IN 'clear_exp_dir'.",err)
                await websocket.send(json.dumps({"error": str(err)}))
        else:
            err_str = "Unsupported command: " + str(data["command"])
            logging.error(err_str)
            await websocket.send(json.dumps({"error": err_str}))


if __name__ == "__main__":
    logging.basicConfig()
    start_server = websockets.serve(identificate, "localhost", 6791)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()

