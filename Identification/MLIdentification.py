import glob
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import pandas as pd

from FrictionIdentification import *


class MLIdentification:
    directory = ""
    file_list = []
    parameters_identified = {}
    res_solutions = []
    res_sigmas = []


    def __init__(self, directory: str):
        if(directory.endswith("/") == False):
            directory += "/"
        self.directory = directory
        self.file_list = glob.glob(directory + "*.csv")


    def compute(self, plot: bool = False,
        confidence: float = 10, max_iterations: int = 1,
        use_filter: bool = False, cutoff_freq: int = 5,
        filter_order: int = 1, normalize: bool = True):

        y = np.zeros((1,1))
        phi = np.zeros((1,1))
        phi_s = np.zeros((1,1))

        for f in self.file_list:
            print(f)
            task = FrictionIdentification(filename=f)
            task.compute(plot=plot, confidence=confidence,
                max_iterations=max_iterations, filter=use_filter,
                cutoff_freq=cutoff_freq, filter_order=filter_order,
                normalize=normalize)
            self.res_solutions.append(task.res_solution)
            # self.res_sigmas.append(task.res_sigma)

            y = np.concatenate([y, task.y_model])
            phi = np.concatenate([phi, task.phi.T])
            phi_s = np.concatenate([phi_s, task.phi.T / task.sigma])

        phi_s = phi_s.T
        retain_indexes = np.arange(0,phi.shape[0]) #[np.newaxis]
        # plt.plot(phi.T)
        # plt.show()
        # plt.plot(y.T)
        # plt.show()

        print("Start loop",phi.shape)
        solution = None

        # identification loop
        for i in range(max_iterations):

            y = y[retain_indexes.T]

            cols = phi.shape[1]
            for i in range(0,cols):
                phi_s[0] = phi_s[0][retain_indexes]

            phi = phi[retain_indexes.T]

            # TODO debug print
            print("ML phi:",phi.shape)
            print("ML phi':",phi.T.shape)
            print("ML y:",y.shape)
            print("ML y':",y.T.shape)
            print("ML phi_s:",phi_s.shape)

            # least squares
            solution = np.linalg.lstsq(phi_s.dot(phi), phi_s.dot(y),rcond=None)[0]
            # lambda = (phi_s * phi) \ phi_s * y
            # var_lambda = inv(phi_s * phi)

            # model approximation of the experiment data
            y_model = phi.dot(solution)

            # error between the model and the experiment
            error = y - y_model

            # error standard deviation
            sigma = np.sqrt(np.var(error))

            # remove data outside the range (assuming Gaussian noise)
            abs_error = abs(error)
            retain_indexes = np.array(list(filter(lambda val: val < sigma*confidence, abs_error)))
            # retain_indexes = find(abs(error) < sigma*confidence)

        return solution


if __name__ == "__main__":
    id = MLIdentification('/home/rudyvic/Documents/Development/Forecast NEXT/forecast_next_backend')
    print(id.compute(confidence=10,plot=False))
    print(id.res_solutions)
    # print(id.parameters_identified)
    # print(id.res_solution,id.res_sigma,type(id.res_solution),type(id.res_sigma))
    # id = Identification()
    # id._least_square_identification()