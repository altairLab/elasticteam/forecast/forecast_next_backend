import os
import platform
import subprocess
from pathlib import Path
import glob
import pandas as pd


def change_friction_log_names(path: str = "/.config/Electron/logs/FrictionIdentification"):
    path = str(Path.home()) + path
    if not path.endswith('/'):
        path = path + '/'
    file_list = glob.glob(path + "*.csv")

    if len(file_list) != 10:
        return "Expected 10 csv, but found " + str(len(file_list)) + " instead."

    file_list.sort()
    counter = 0
    for f in file_list:
        if int(f[-5:-4]) != counter:
            return "Expected 10 csv with increasing numeration."
        counter += 1

    try:
        for f in file_list:
            old_name = f.split('/')[-1]
            dir = f[:-len(old_name)]
            counter = int(old_name[-5:-4])
            new_counter = (counter % 5) + 1
            name = "_pos"
            if counter >= 5:
                name = "_neg"
            new_name = old_name[:-5] + name + str(new_counter) + ".csv"
            os.rename(path + old_name, path + new_name)
    except Exception as e:
        return str(e)

    return ""


def open_exp_dir(path: str = "/.config/Electron/logs/FrictionIdentification"):
    path = str(Path.home()) + path
    try:
        if platform.system() == "Windows":
            os.startfile(path)
        elif platform.system() == "Darwin":
            subprocess.Popen(["open", path])
        else:
            subprocess.Popen(["xdg-open", path])
    except Exception as e:
        return str(e)
    return ""


def clear_exp_dir(path: str = "/.config/Electron/logs/FrictionIdentification"):
    path = str(Path.home()) + path

    if not path.endswith('/'):
        path = path + '/'
    file_list = glob.glob(path + "*.csv")

    try:
        for f in file_list:
            os.remove(f)
    except Exception as e:
        return str(e)

    return ""


def get_csv_params(path: str) -> list:
    t_list = []
    try:
        table = pd.read_csv(path)
        t_list = list(table.columns)
    except Exception as e:
        return []

    counter = len(t_list)
    for t in t_list:
        try:
            float(t)
            counter -= 1
        except:
            pass

    # if all the columns are floats, then the table doesn't have names
    if counter == 0:
        return []
    return t_list


if __name__ == "__main__":
    # print(open_exp_dir())
    # print(clear_exp_dir())
    print(get_csv_params("/home/rudyvic/Desktop/EMANUELE/EnvironmentIdentification/Maxon_DCX22L/inertia/kdes_1_bdes_0.01_jdes0.02_start2_end9.csv"))
    print(get_csv_params("/home/rudyvic/.config/Electron/logs/FrictionIdentification/log_neg4.csv"))