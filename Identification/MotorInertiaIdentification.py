from Identification import *


class MotorInertiaIdentification(Identification):
    required_names = ['t', 'tauSensor', 'thetaM', 'dthetaM', 'ddthetaM']

    def __init__(self, filename: str):
        super(MotorInertiaIdentification,self).__init__(filename)


    @staticmethod
    def get_required_names():
        return __class__.required_names


    def _load_table(self):
        table = self.table

        t = table['t'].to_numpy()
        tauSensor = table['tauSensor'].to_numpy()
        thetaM = table['thetaM'].to_numpy()
        dthetaM = table['dthetaM'].to_numpy()
        ddthetaM = table['ddthetaM'].to_numpy()

        y = tauSensor
        phi = np.array([
            ddthetaM,
            dthetaM,
            np.sign(dthetaM),
            np.ones(thetaM.shape)
        ])

        return [y, phi, t]


    def _set_parameters_identified(self, solution):
        self.parameters_identified["Motor inertia"] = solution[0]


if __name__ == "__main__":
    # TODO not tested yet
    print(MotorInertiaIdentification.get_required_names())
    id = MotorInertiaIdentification('motor_inertia.csv')
    id.compute()
    print(id.parameters_identified)
    print(id.res_solution,id.res_sigma)