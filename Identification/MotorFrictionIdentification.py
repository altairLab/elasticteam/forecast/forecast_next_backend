from Identification import *
import numpy as np


class MotorFrictionIdentification(Identification):
    required_names = ['t', 'dthetaM', 'tauM']

    def __init__(self, filename: str):
        super(MotorFrictionIdentification,self).__init__(filename)


    @staticmethod
    def get_required_names():
        return __class__.required_names


    def _load_table(self):
        table = self.table

        t = table['t'].to_numpy()
        dthetaM = table['dthetaM'].to_numpy()
        tauM = table['tauM'].to_numpy()
        
        dthetaM_pos = dthetaM.copy()
        dthetaM_neg = dthetaM.copy()
        sign_dthetaM_pos = dthetaM.copy()
        sign_dthetaM_neg = dthetaM.copy()
        eps = 0.2

        for i in range(dthetaM.shape[0]):
            if abs(dthetaM[i]) < eps:
                dthetaM_neg[i] = 0
                sign_dthetaM_neg[i] = 0
                sign_dthetaM_pos[i] = 0
            elif dthetaM[i] >= eps:
                dthetaM_neg[i] = 0
                sign_dthetaM_neg[i] = 0
                sign_dthetaM_pos[i] = 1
            elif dthetaM[i] <= eps:
                dthetaM_pos[i] = 0
                sign_dthetaM_pos[i] = 0
                sign_dthetaM_neg[i] = -1


        y = tauM
        phi = np.array([
            # thetaE,
            # np.ones(dthetaM.shape),
            # np.power(thetaE,2),
            # np.power(thetaE,3),
            # np.sign(dthetaM),
            dthetaM_pos,
            dthetaM_neg,
            sign_dthetaM_pos,
            sign_dthetaM_neg,
        ])

        return [y, phi, t]


    def _set_parameters_identified(self, solution):
        self.parameters_identified["friction"] = solution[0]


if __name__ == "__main__":
    id = MotorFrictionIdentification('/home/altair/forecast/output.csv')
    # print(id.filename)
    # my_data = read_csv('pose_2.csv')
    id.compute(confidence=10,plot=False)
    print(id.parameters_identified)
    print(id.res_solution,id.res_sigma,type(id.res_solution),type(id.res_sigma))
    print(id.parameters_identified)
    # id = Identification()
    # id._least_square_identification()