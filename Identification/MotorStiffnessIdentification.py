from Identification import *


class MotorStiffnessIdentification(Identification):
    required_names = ['t', 'thetaM', 'tauSensor']

    def __init__(self, filename: str):
        super(MotorStiffnessIdentification,self).__init__(filename)


    @staticmethod
    def get_required_names():
        return __class__.required_names


    def _load_table(self):
        table = self.table

        t = table['t'].to_numpy()
        tauSensor = table['tauSensor'].to_numpy()
        thetaM = table['thetaM'].to_numpy()

        y = tauSensor
        phi = np.array([
            thetaM,
            np.ones(thetaM.shape),
        ])

        return [y, phi, t]


    def _set_parameters_identified(self, solution):
        self.parameters_identified["motor stiffness"] = solution[0]


if __name__ == "__main__":
    # TODO not tested yet
    id = MotorStiffnessIdentification('/home/rudyvic/Desktop/EMANUELE/MotorIdentification/data/log256.csv')
    id.compute()
    print(id.parameters_identified)
    print(id.res_solution,id.res_sigma,type(id.res_solution),type(id.res_sigma))