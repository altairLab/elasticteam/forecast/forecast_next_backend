from Identification import *
import numpy as np
import glob
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal


class TestbedFrictionIdentification(Identification):
    required_names = ['t', 'thetaE', 'dthetaE', 'tauSensor']

    def __init__(self, filename: str):
        super(TestbedFrictionIdentification,self).__init__(filename)


    @staticmethod
    def get_required_names():
        return __class__.required_names


    def _load_table(self):
        table = self.table

        t = table['t'].to_numpy()
        thetaE = table['thetaE'].to_numpy()
        dthetaE = table['dthetaE'].to_numpy()
        tauSensor = table['tauSensor'].to_numpy()
        omega = 1

        y = tauSensor
        phi = np.array([
            dthetaE,
            np.ones(thetaE.shape),
            np.cos(thetaE * omega),
            np.sin(thetaE * omega),
            np.cos(2.0 * thetaE * omega),
            np.sin(2.0 * thetaE * omega),
            np.cos(3.0 * thetaE * omega),
            np.sin(3.0 * thetaE * omega),
            np.cos(4.0 * thetaE * omega),
            np.sin(4.0 * thetaE * omega),
            np.cos(5.0 * thetaE * omega),
            np.sin(5.0 * thetaE * omega),
            np.cos(6.0 * thetaE * omega),
            np.sin(6.0 * thetaE * omega),
            np.cos(7.0 * thetaE * omega),
            np.sin(7.0 * thetaE * omega),
            np.cos(8.0 * thetaE * omega),
            np.sin(8.0 * thetaE * omega),
        ])

        return [y, phi, t]


    def _set_parameters_identified(self, solution):
        self.parameters_identified["viscous"] = solution[0]
        self.parameters_identified["a0"] = solution[1]
        self.parameters_identified["a1"] = solution[2]
        self.parameters_identified["b1"] = solution[3]
        self.parameters_identified["a2"] = solution[4]
        self.parameters_identified["b2"] = solution[5]
        self.parameters_identified["a3"] = solution[6]
        self.parameters_identified["b3"] = solution[7]
        self.parameters_identified["a4"] = solution[8]
        self.parameters_identified["b4"] = solution[9]
        self.parameters_identified["a5"] = solution[10]
        self.parameters_identified["b5"] = solution[11]
        self.parameters_identified["a6"] = solution[12]
        self.parameters_identified["b6"] = solution[13]
        self.parameters_identified["a7"] = solution[14]
        self.parameters_identified["b7"] = solution[15]
        self.parameters_identified["a8"] = solution[16]
        self.parameters_identified["b8"] = solution[17]


def compute_friction_identification_one_direction(dir: str, pattern: str) -> str:
    file_list = glob.glob(dir + f"*{pattern}*.csv")

    if len(file_list) != 5:
        return f"Expected 5 csv '*{pattern}*', but found {len(file_list)} instead."

    file_list.sort()

    start_time = -1
    end_time = -1

    series_thetaE = []
    series_dthetaE = []
    series_tauSensor = []
    series_time_vector = []

    try:
        for j in range(len(file_list)):
            data = pd.read_csv(file_list[j])

            # take data from 2*pi to 12*pi, that is equal to 5 rotations
            min_index = -1
            max_index = -1

            for i in range(len(data['thetaE'])):
                if min_index == -1 and abs(data['thetaE'].iloc[i]) > 2*np.pi:
                    min_index = i

                if min_index != -1 and abs(data['thetaE'].iloc[i]) > 12*np.pi:
                    max_index = i
                    break

            start_time = min_index
            end_time = max_index

            print(f"{file_list[j]} \t start_time {start_time} \t end_time {end_time} \t delta {end_time - start_time}")

            # initialize the time vector
            time_vector = data['t'].iloc[start_time:end_time].copy() - data['t'].iloc[0]
            last_time_value = -1
            if len(series_time_vector) == 0:
                last_time_value = 0
            else:
                last_time_value = series_time_vector[-1]

            time_vector = time_vector + last_time_value

            ts = abs(np.mean(np.diff(time_vector)))
            fs = 1.0 / ts
            cutoff_freq = 10.0
            half_sampling_freq = fs / 2.0
            [B,A] = signal.butter(4,cutoff_freq/half_sampling_freq)

            thetaE = data['thetaE'].iloc[start_time:end_time].copy()
            thetaE = signal.lfilter(B,A, thetaE)
            thetaE = np.mod(thetaE, 2.0*np.pi)

            dthetaE = data['dthetaE'].iloc[start_time:end_time].copy()
            # dthetaE = signal.lfilter(B,A, dthetaE)
            tauSensor = data['tauSensor'].iloc[start_time:end_time].copy()
            tauSensor = signal.lfilter(B,A, tauSensor)

            series_thetaE = np.append(series_thetaE, thetaE)
            series_dthetaE = np.append(series_dthetaE, dthetaE)
            series_tauSensor = np.append(series_tauSensor, tauSensor)
            series_time_vector = np.append(series_time_vector, time_vector)

        df = pd.DataFrame({'t': series_time_vector,
                            'thetaE': series_thetaE,
                            'dthetaE': series_dthetaE,
                            'tauSensor': series_tauSensor})

        csv_path = str(Path.home()) + f"/.config/Electron/logs/out_friction{pattern}.csv"
        df.to_csv(csv_path,index=False)
    except Exception as e:
        return str(e)
    return ""


def compute_testbed_friction_identification(dir: str) -> str:
    dir = str(Path.home()) + dir
    if not dir.endswith('/'):
        dir = dir + '/'

    # negative experiments
    err = compute_friction_identification_one_direction(dir,"_neg")
    if len(err) != 0:
        return err

    # positive experiments
    err = compute_friction_identification_one_direction(dir,"_pos")
    if len(err) != 0:
        return err
    return ""


if __name__ == "__main__":
    print(compute_testbed_friction_identification("/.config/Electron/logs/FrictionIdentification"))
    # compute_testbed_friction_identification("/Desktop/PROVATEO")

    id = TestbedFrictionIdentification('out_friction_neg.csv')
    id.compute(confidence=10,plot=False)
    # print(id.parameters_identified)
    # print(id.res_solution,id.res_sigma,type(id.res_solution),type(id.res_sigma))
    for p in id.parameters_identified.keys():
        print(f"{p}: {id.parameters_identified[p]}")

    id = TestbedFrictionIdentification('out_friction_pos.csv')
    id.compute(confidence=10,plot=False)
    # print(id.parameters_identified)
    # print(id.res_solution,id.res_sigma,type(id.res_solution),type(id.res_sigma))
    for p in id.parameters_identified.keys():
        print(f"{p}: {id.parameters_identified[p]}")
    # id = Identification()
    # id._least_square_identification()