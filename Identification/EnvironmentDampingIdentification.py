from Identification import *


class EnvironmentDampingIdentification(Identification):
    required_names = ['t', 'thetaE', 'dthetaE', 'tauSensor']

    def __init__(self, filename: str):
        super(EnvironmentDampingIdentification,self).__init__(filename)


    @staticmethod
    def get_required_names():
        return __class__.required_names


    def _load_table(self):
        table = self.table

        t = table['t'].to_numpy()
        tauSensor = table['tauSensor'].to_numpy()
        thetaE = table['thetaE'].to_numpy()
        dthetaE = table['dthetaE'].to_numpy()

        y = tauSensor
        phi = np.array([
            thetaE,
            dthetaE,
            np.ones(dthetaE.shape)
        ])

        return [y, phi, t]


    def _set_parameters_identified(self, solution):
        self.parameters_identified["environment damping"] = solution[1]


if __name__ == "__main__":
    id = EnvironmentDampingIdentification('env_damping.csv')
    # print(id.filename)
    # my_data = read_csv('pose_2.csv')
    id.compute()
    print(id.parameters_identified)
    print(id.res_solution,id.res_sigma,type(id.res_solution),type(id.res_sigma))
    # id = Identification()
    # id._least_square_identification()