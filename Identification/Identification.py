from shutil import ExecError
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import pandas as pd


class Identification:
    table = pd.DataFrame()
    filename = ""
    required_names = list()
    parameters_identified = {}
    res_solution = list()
    res_sigma = list()
    phi = pd.DataFrame()
    sigma = pd.DataFrame()
    y_model = pd.DataFrame()


    def __init__(self, filename: str):
        if self.__class__.__name__ == 'Identification':
            raise NotImplementedError("Can't instantiate Identification class")
        if not self.required_names:
            raise NotImplementedError("self.required_names not set. "
                + "Set it in the __init__().")

        self.filename = filename
        self.table = self.read_csv()


    def read_csv(self) -> np.ndarray:
        table = pd.read_csv(self.filename)

        # rename the column 'time' to 't' (if present)
        table.rename({'time': 't'},inplace=True,axis='columns')

        # remove the leading and trailing spaces in the columns names
        for x in list(table.columns):
            table.rename({x: x.strip()},inplace=True,axis="columns")

        # remove the non-necessary columns
        table = table[table.columns.intersection(self.required_names)]

        # check if there remaining columns are right
        for name in self.required_names:
            if name not in table.columns:
                raise ExecError("Missing a required parameter: " + name)

        # offset the time to make it start from 0
        table['t'] = table['t'] - table['t'][0]

        # set the start time
        # TODO

        # set the end time
        # TODO

        return table


    def _load_table(self) -> list:
        raise NotImplementedError("Implement _load_table() in "
            + self.__class__.__name__)


    def _set_parameters_identified(self, solution):
        raise NotImplementedError("Implement _set_parameters_identified() in "
            + self.__class__.__name__)


    def compute(self, plot: bool = False,
        confidence: float = 10, max_iterations: int = 1,
        filter: bool = False, cutoff_freq: int = 5,
        filter_order: int = 1, normalize: bool = True):

        # filter the table if the flag is True
        if filter:
            self._filter_table(cutoff_freq=cutoff_freq,filter_order=filter_order)

        # load the y and phi from the table (this must be a method implemented
        # in the child class)
        [y, phi, t] = self._load_table()

        # normalize y and phi if the flag is True
        if normalize:
            kn = 1/np.max(y)
            y = kn * y
            phi = kn * phi

        # least squares identification
        [solution, sigma, y_model, error] = self._LSIdentification(phi, y, t, confidence, max_iterations)

        # TODO debug print
        print("Error:",error.shape)

        # convert the results to an np.ndarray()
        solution = np.array(solution)
        sigma = np.array(sigma)

        # set the lambda results
        self.res_solution.clear()
        solution = np.ndarray.flatten(solution).tolist()
        self.res_solution += solution

        # set the sigma results
        self.res_sigma.clear()
        sigma = np.ndarray.flatten(sigma).tolist()
        self.res_sigma += sigma

        # TODO debug print
        # print("Lambda: " + str(solution))
        # print("Sigma: " + str(sigma))
        # print("Y model: " + str(y_model))

        # set the identified parameters (this must be a method implemented in
        # the child class)
        self._set_parameters_identified(solution)

        # plot the model if the flag is True
        if plot:
            plt.plot(t,y,linewidth=0.5)
            plt.plot(t,y_model,'g',linewidth=1.0)
            plt.plot(t,error,'r',linewidth=1.0)
            plt.legend(['y','model','error'])
            plt.show()


    def _filter_table(self, cutoff_freq: int = 5, filter_order: int = 1):
        ts = np.mean(np.diff(self.table['t']))
        print(ts)
        half_sampling_freq = float(1/(2*ts))
        [B,A] = signal.butter(filter_order,cutoff_freq/half_sampling_freq)

        for col in self.table.columns:
            if col != 't':
                self.table[col] = signal.filtfilt(B,A, self.table[col])


    def _LSIdentification(self, phi: np.ndarray, y: np.ndarray,
        t: np.ndarray, confidence: float, max_iterations: int):
        # Least Squares identification: a least squares procedure that iterates
        # several times and eliminates outliers.
        #
        # confidence: used to remove the outliers
        # max_iterations: maximum number of iterations
        #
        # EXAMPLES:
        # [lambda sigma] = LSIdentification(phi, y, t, 3, 14)
        # Does 14 cycles and removes ouliers out of 3 sigma confidence

        retain_indexes = np.arange(0,phi.shape[1])[np.newaxis]

        # TODO debug print
        print("retain_indexes:",retain_indexes.shape)

        # identification loop
        for i in range(0,max_iterations):

            y = y[retain_indexes.T]

            cols = phi.shape[0]
            for i in range(0,cols):
                phi[i] = phi[i][retain_indexes]

            t = t[retain_indexes.T]

            # TODO debug print
            print("phi:",phi.shape)
            print("phi':",phi.T.shape)
            print("y:",y.shape)
            print("y':",y.T.shape)
            print("t:",t.shape)

            # least squares
            solution = np.linalg.lstsq(phi.dot(phi.T), phi.dot(y),rcond=None)[0]
            # var_lambda = np.linalg.inv(phi.dot(phi.T))

            # model approximation of the experiment data
            y_model = phi.T.dot(solution)

            # error between the model and the experiment
            error = y - y_model

            # error standard deviation
            sigma = np.sqrt(np.var(error))

            # remove data outside the range (assuming Gaussian noise)
            abs_error = abs(error)
            retain_indexes = np.array(list(filter(lambda val: val < sigma*confidence, abs_error)))

            # TODO debug print
            print("retain_indexes:",retain_indexes.shape)

            if(retain_indexes.shape[1] == y.shape[1]):
                break

        # save some results in the properties
        self.sigma = sigma.copy()
        self.y_model = y_model.copy()
        self.phi = phi.copy()

        return [solution, sigma, y_model, error]

