from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

long_description = (here / 'README.md').read_text(encoding='utf-8')

def glob_fix(package_name, glob):
    # This assumes setup.py lives in the folder that contains the package
    package_path = pathlib.Path(f'./{package_name}').resolve()
    return [str(path.relative_to(package_path))
            for path in package_path.glob(glob)]

setup(
    name='forecast-next-backend',
    version='0.1.0',
    description='Evaluation and Identification backend of Forecast NEXT',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/altairLab/elasticteam/forecast/forecast_next_backend',
    author='Rudy Vicario',
    author_email='visotruce@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],
    packages=find_packages(),
    python_requires='>=3.5',
    install_requires=[
        "numpy",
        "matplotlib",
        "scipy",
        "pandas",
        "websockets",
        "asyncio",
        "logging",
    ],
    project_urls={
        'Source': 'https://gitlab.com/altairLab/elasticteam/forecast/forecast_next_backend',
    },
)
