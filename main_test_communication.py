import json

import websockets
import asyncio

from enum import Enum

class SerialPacket:
    def _get_id(self):
        return self.__id
    def _set_id(self, value):
        if not isinstance(value, int):
            raise TypeError("id must be set to an integer")
        self.__id = value
    id = property(_get_id, _set_id)

    def _get_payload(self):
        return self.__payload
    def _set_payload(self, value):
        if not isinstance(value, bytearray):
            raise TypeError("payload must be set to a bytearray")
        self.__payload = value
    payload = property(_get_payload, _set_payload)


class PacketID(Enum):
    ACK = (0x06)
    NACK = (0x0F)

    # Interrogations
    HW_INFO = (0x20)
    VER = (0x21)
    CTRL_LIST = (0x22)
    REF_LIST = (0x23)
    CTRL_PARAMS = (0x24)
    REF_PARAMS = (0x25)
    CTRL_LOGS = (0x26)
    READY = (0x27)

    # Commands
    CTRL_SET = (0x40)
    LOG_SET = (0x41)
    REF_SET = (0x42)
    START_LOOP = (0x43)
    STANDBY = (0x44)

    # No response packets
    LOG = (0x60)
    ERROR = (0x61)

async def handleHWInfo(websocket):
    print('HW info')
    motors_num = 2
    log_list = "t, tau, theta"
    response = bytearray([PacketID.ACK.value, PacketID.HW_INFO.value, motors_num])
    response.extend(log_list.encode('utf-8'))
    await websocket.send(response)

async def handleFWInfo(websocket):
    print('FW version')
    response = bytearray([PacketID.ACK.value,PacketID.VER.value])
    response.extend('v1.0.0'.encode('utf-8'))
    await websocket.send(response)

async def handleCtrlList(websocket):
    print('CTRL list')
    ctrl_list = "PID, Velocity"
    response = bytearray([PacketID.ACK.value, PacketID.CTRL_LIST.value])
    response.extend(ctrl_list.encode('utf-8'))
    await websocket.send(response)

async def handleRefList(websocket):
    print('REF list')
    refs_list = "step, ramp, sin, sweep"
    response = bytearray([PacketID.ACK.value, PacketID.REF_LIST.value])
    response.extend(refs_list.encode('utf-8'))
    await websocket.send(response)

async def handleCtrlParams(websocket, ctrl_id: str):
    print('CTRL params of ' + ctrl_id)
    params_list = "KP, KI, KD"
    response = bytearray([PacketID.ACK.value, PacketID.CTRL_PARAMS.value])
    response.extend(params_list.encode('utf-8'))
    await websocket.send(response)

async def handleRefParams(websocket, ref_id: str):
    print('REF params of ' + ref_id)
    params_list = "KP, KI, KD"
    response = bytearray([PacketID.ACK.value, PacketID.REF_PARAMS.value])
    response.extend(params_list.encode('utf-8'))
    await websocket.send(response)

async def handleCtrlLogs(websocket, ctrl_id: str):
    print('CTRL logs of ' + ctrl_id)
    params_list = "KP, KI, KD"
    response = bytearray([PacketID.ACK.value, PacketID.CTRL_LOGS.value])
    response.extend(params_list.encode('utf-8'))
    await websocket.send(response)

async def handleReady(websocket):
    print('READY')

async def handleCtrlSet(websocket, payload: bytearray):
    print('CTRL set')
    id_ctrl = payload['id_ctrl']
    num_motors = payload['num_motors']
    parameters = payload['parameters']
    print(id_ctrl,num_motors,parameters)

async def server_function(websocket, path):
    async for message in websocket:
        rcv_msg = json.loads(message)
        id = rcv_msg['id']
        if id == PacketID.HW_INFO.value:
            await handleHWInfo(websocket)
        elif id == PacketID.VER.value:
            await handleFWInfo(websocket)
        elif id == PacketID.CTRL_LIST.value:
            await handleCtrlList(websocket)
        elif id == PacketID.REF_LIST.value:
            await handleRefList(websocket)
        elif id == PacketID.CTRL_PARAMS.value:
            await handleCtrlParams(websocket,rcv_msg['payload'])
        elif id == PacketID.REF_PARAMS.value:
            await handleRefParams(websocket,rcv_msg['payload'])
        elif id == PacketID.CTRL_LOGS.value:
            await handleCtrlLogs(websocket,rcv_msg['payload'])
        elif id == PacketID.READY.value:
            await handleReady(websocket)
        elif id == PacketID.CTRL_SET.value:
            await handleCtrlSet(websocket,rcv_msg['payload'])
        else:
            print('Not supported ID (0x{:x})'.format(id))


if __name__ == "__main__":
    # websocket
    start_server = websockets.serve(server_function, "localhost", 6789)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
